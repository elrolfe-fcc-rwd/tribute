# [Build a Tribute Page](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-tribute-page)
## [Free Code Camp](https://www.freecodecamp.org) - Responsive Web Design Project

## Objective
Build a tribute page that is functionally similar to this [example page](https://codepen.io/freeCodeCamp/full/zNqgVx).

The [completed page](https://elrolfe-fcc-rwd.gitlab.io/tribute) was coded with HTML and CSS.

## User Stories
This tribute page fulfills the following user stories provided by FreeCodeCamp.org:
1. My tribute page should have an element with a corresponding `id="main"`, which contains all other elements.
2. I should see an element with a corresponding `id="title"`, which contains a text string that describes the subject of the tribute page (e.g. "Dr. Norman Borlaug")
3. I should see a `div` element with a corresponding `id="img-div"`.
4. Within the `img-div` element, I should see an `img` element with a corresponding `id="image"`.
5. Within the `img-div` element, I should see an element with a corresponding `id="img-caption"` that contains textual content describing the image showin in `img-div`.
6. I should see an element with a corresponding `id="tribute-info"`, which contains textual content describing the subject of the tribute page.
7. I should see an `a` element with a corresponding `id="tribute-link"`, which links to an outside site that contains additional information about the subject of the tribute page.
8. The `img` element should responsively resize, relative to the width of its parent element, without exceeding its original size.